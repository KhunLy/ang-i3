import { Component } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  items: NbMenuItem[] = [
    { title: 'Acceuil', icon: 'home', link: '/home' },
    { title: 'About', icon: 'star', link: '/about' },
    { title: 'Exos', icon: 'book', children: [
      { title: 'Exercice 1 - Chat', icon: 'message-circle', link: '/exo/exo1' }
    ] },
    { title: 'Produit', icon: 'moon', link: '/product' },
    { title: 'Ajouter un Produit', icon: 'sun', link: '/product-add' },
  ];
  
  private states: string[] = ['compacted', 'expanded', 'collapsed'];

  private current: number = 0;

  get state(): string {
    return this.states[this.current];
  }
  
  menu() {
    // this.current++;
    // this.current %= this.states.length;

    this.current = (this.current + 1) % this.states.length;
  }
}
