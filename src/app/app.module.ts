import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbCardModule, NbButtonModule, NbInputModule, NbSelectModule, NbDatepickerModule, NbDialogModule, NbCheckboxModule, NbToastrModule, NbIconModule, NbChatModule } from '@nebular/theme';
import { ExercicesComponent } from './pages/exercices/exercices.component';
import { Exercice1Component } from './pages/exercices/exercice1/exercice1.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TimestampToDatePipe } from './pipes/timestamp-to-date.pipe';
import { LoginComponent } from './pages/login/login.component';
import { ProductComponent } from './pages/product/product.component';
import { ProductAddComponent } from './pages/product-add/product-add.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProductService } from './services/product.service';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { LoaderComponent } from './components/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ExercicesComponent,
    Exercice1Component,
    TimestampToDatePipe,
    LoginComponent,
    ProductComponent,
    ProductAddComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    HttpClientModule,

    FormsModule, // binding 2 ways
    ReactiveFormsModule, // formulaire plus complexe

    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,

    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbCheckboxModule,
    NbToastrModule.forRoot(),
    NbIconModule,
    NbChatModule.forRoot(),

    AngularFireModule.initializeApp(environment.firebase),

    NbEvaIconsModule,
    AppRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
