import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { NbToastrService } from '@nebular/theme';
import { MessageModel } from 'src/app/models/message.model';

@Component({
  selector: 'app-exercice1',
  templateUrl: './exercice1.component.html',
  styleUrls: ['./exercice1.component.scss']
})
export class Exercice1Component implements OnInit {

  messages: MessageModel[];

  constructor(
    private db: AngularFireDatabase,
    private toastr: NbToastrService,
  ) { }

  ngOnInit(): void {
    this.db.object<MessageModel[]>('messages').valueChanges().subscribe(data => {
      let list = (data || []).filter(x => x !== null);
      if(this.messages) {
        // si les messages sont intialisés
        let newMessage = list[list.length-1];
        if(newMessage.auteur !== 'Khun') {
          // on ajoute le dernier message dans le cas ou je n'en suis pas l'auteur
          this.messages.push(newMessage);
          this.toastr.info(
            'Vous avez reçu un message de la part de ' + newMessage.auteur, 
            'Nouveau message !!!'
          );
        }
      }
      else{
        // on initialise les messages
        this.messages = list;
      }
    });
  }

  send(event) {
    this.messages.push({
      auteur: 'Khun',
      contenu: event.message,
      date: Date.now()
    });
    this.db.object('messages').set(this.messages);
  }
}
